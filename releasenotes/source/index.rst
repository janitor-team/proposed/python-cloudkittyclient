Welcome to CloudKitty Client Release Notes documentation!
=========================================================

Contents
========

.. toctree::
   :maxdepth: 2

   unreleased
   yoga
   xena
   wallaby
   victoria
   ussuri
   train
   stein
   rocky
   queens

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
